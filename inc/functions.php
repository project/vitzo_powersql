<?php
/**
 * PowerSQL functions for Drupal modules
 * 
*  @copyright GPL (http://www.gnu.org/copyleft/gpl.html)
 * @author Ken De Volder for Vitzo Limited (www.vitzo.com)
 */

/**
 * Convert an array to SQL code
 * 
 * @param array Contains the name => value pairs to be updated
 * @param string If the first param is a nested array this delimiter will be used to seperate the values
 */
function powersql_array2sql(array $array, $delimiter = ", ")
{
	$query = "";
	foreach ($array as $key => $value)
	{
		if (!is_array($value))
		{
			$query .= "$key='$value'$delimiter";
		}
		else // Implode the array
		{
			foreach ($value as $tKey => $tValue)
			{
				// Wrap each value with quotes
				$value[$tKey] = "'$value[$tKey]'";
			}
			$query .= "$key IN (".implode(",",$value).")$delimiter";
		}
	}
	return substr($query,0,strlen($query)-strlen($delimiter));
}

/**
 * Get a single variable from the database
 * 
 * @param string The query to be executed. Only the first result will be returned.
 */
function db_fetch_var($query)
{
	if ($result = db_fetch_array(db_query($query)))
	{
		return array_shift($result);
	}
	return false;
}

/**
 * Insert or update the values in $array depending on the $where value
 * 
 * @param string Name of the table to update or insert into
 * @param array Contains the name => value pairs to be updated
 * @param mixed Use NULL to always update or an SQL WHERE query to insert when no match is found
 */
function db_persist($table, array $array, $where = null)
{
	if (!empty($where) && $where != null && !db_fetch_var("SELECT COUNT(*) FROM $table WHERE $where"))
	{
		$where = null; // Insert new row 
	}
	
	return db_update_array($table,$array,$where);
}

/**
 * Insert an array into the active database
 * 
 * @param string Name of the table to insert into
 * @param array Contains the name => value pairs to be inserted
 */
function db_insert_array($table, array $array)
{
	return db_update_array($table,$array);
}

/**
 * Update the values in $array in $table depending on the $where value
 * 
 * @param string Name of the table to update
 * @param array Contains the name => value pairs to be updated
 * @param mixed SQL WHERE query to insert when no match is found
 * @return Returns the last inserted ID if available or FALSE if not
 */
function db_update_array($table, array $array, $where = null)
{
	$query = powersql_array2sql($array);
	
	if (empty($where)) // Insert
	{
		if ($result = db_query("INSERT INTO $table SET ".$query))
		{
			// Return the ID of the latest row
			$result = db_last_insert_id($table, "id");
			
			// Some tables don't use ID's
			if ($result == 0) return true;
		}
	} 
	else // Update existing entry
	{
		if (db_query("UPDATE $table SET ".$query." WHERE $where"))
		{
			return true;
		}
	}
	return false;
}
